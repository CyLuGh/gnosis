# .NET

## Framework

[[manual_clickonce|Manual ClickOnce (with Mage)]]
[[netsh]]

## Core

[[publish|dotnet publish]]

## Snippets and useful code

[[IObservableExtensions]]
[[ReflectionTypeGenerator]]

## Libraries

### MVVM

|Name|url|
|--:|--|
| ReactiveUI | https://github.com/reactiveui/ReactiveUI |

### Tools

|Name|url|
|--:|--|
| LanguageExt | https://github.com/louthy/language-ext |
| MoreLinq | https://github.com/morelinq/MoreLINQ |

### UI

|Name|url|
|--:|--|
| MahApps | https://github.com/MahApps/MahApps.Metro |
| MaterialDesignInXaml| https://github.com/MaterialDesignInXAML/MaterialDesignInXamlToolkit |
| WPF.DragDrop | https://github.com/punker76/gong-wpf-dragdrop |