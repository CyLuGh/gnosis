# Publish

`dotnet publish` - Publishes the application and its dependencies to a folder for deployment to a hosting system.

```powershell
dotnet publish -r win10-x64 -p:PublishSingleFile=false -p:PublishTrimmed=false --self-contained true -p:PublishReadyToRun=false --configuration Release
```

## Runtime
-r / -runtime (see https://docs.microsoft.com/en-us/dotnet/core/rid-catalog)
  
Runtime to be targeted: beware that win-x64 and win10-x64 aren't the same ones! Some libraries may be missing when using the wrong runtime.

## ClickOnce

Those file may be use to create a ClickOnce distributable (see https://github.com/dotnet/deployment-tools/blob/main/Documentation/dotnet-mage/README.md)

To install dotnet mage

```powershell
dotnet tool install --global Microsoft.DotNet.Mage --version 6.0.1
```

- Copy the files to `xxx/files`
- Add `MyApp.ico` to files
- Open a terminal and go to xxx directory
- Create Launcher

```powershell
dotnet mage -al MyApp.exe -td files
```

- Create Application manifest

```powershell
dotnet mage -new Application -t files\MyApp.manifest -fd files -v 1.0.0.1 -if MyApp.ico
```

- Create Deployment manifest

```powershell
dotnet mage -new Deployment -Install true -n "Nice app name" -pub "Publisher name" -v 1.0.0.1 -AppManifest files\MyApp.manifest -t MyApp.application
```

### Updates

- Update Application manifest

```powershell
dotnet mage -update files\MyApp.manifest -v 1.0.0.2 -FromDirectory .\files
```

- Update Deployment manifest

```powershell
dotnet mage -update MyApp.application -v 1.0.0.2 -AppManifest files\MyApp.manifest
```
