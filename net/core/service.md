# Service

Add nuget package `Microsoft.Extensions.Hosting.WindowsServices` and add `UseWindowsService()`

```csharp
public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
```

Install as service with `sc tool`

```powershell
sc create MyService binPath=C:\MyService\MyService.exe
```

See: https://docs.microsoft.com/en-us/dotnet/architecture/grpc-for-wcf-developers/self-hosted
