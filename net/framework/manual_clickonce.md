# Manual ClickOnce

If, for any reason, ClickOnce files generated from Visual Studio would stop working, there's a way to generate it with other tools.

1. Copy files to be deployed:
   - exe
   - dll
   - config
   - ico
2. Launch `mageui` from Developer Command Prompt and create a new application manifest
   - Name: Set name, version and processor 
   - Files: Add directory where files were copied, don't forget to check the .deploy option.
3. Save manifest (don't sign it if prompted)
4. Edit manifest: application icon is missing in manifest, information should be added on the line before `<application />`
   ```xml
   <description asmv2:iconFile="xxx.ico" xmlns="urn:schemas-microsoft-com:asm.v1" />
   ```
5. Create or update deployment manifest.
    ```powershell
    mage -update xxx.application -appmanifest <path>\xxx.manifest
    ```
6. Open deployment manifest with `mageui`
    - Name: Set version
    - Description: Set publisher
7. Save manifest (don't sign it if prompted)
8. Upload xxx.application and *.deploy files

   