# netsh

Use `netsh http` to query and configure HTTP.sys settings and parameters.

Before deploying a Windows service that will bind to an HTTP/HTTPS port, it requires to be authorized through `netsh` command.

```powershell
netsh http add urlacl url=http://+:<PORT>/<SERVICE> user=<DOMAIN>\<USERNAME>
```

More information:
https://docs.microsoft.com/en-us/windows-server/networking/technologies/netsh/netsh-http