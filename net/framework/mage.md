# Mage

The Manifest Generation and Editing Tool (Mage.exe) is a command-line tool that supports the creation and editing of application and deployment manifests. As a command-line tool, Mage.exe can be run from both batch scripts and other Windows-based applications, including ASP.NET applications.

https://docs.microsoft.com/en-us/dotnet/framework/tools/mage-exe-manifest-generation-and-editing-tool

## Application

```cmd
mage -New Application -FromDirectory $path$ -IconFile $*.ico$ -Name *name* -Processor "proc" -ToFile $file$ -TrustLevel FullTrust -Version N.N.N.N
```